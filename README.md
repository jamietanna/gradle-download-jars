# Retrieving All Dependencies Required by a JAR at Runtime

This is a sample project to go hand-in-hand with the blog post [Retrieving All Dependencies Required by a JAR at Runtime](https://www.jvt.me/posts/2021/12/16/download-all-dependencies-gradle/).

# Usage

```sh
./gradlew clean buildZip
# then see what files are produced:
unzip -l build/distributions/gradle-jars.zip
```
